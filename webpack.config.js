const package = require("./package.json")

const path = require("path")
const notifier = require("node-notifier")

const convert = require("koa-connect")
const history = require("connect-history-api-fallback")

const VueLoaderPlugin = require("vue-loader/lib/plugin")
const MiniCSSExtractPlugin = require("mini-css-extract-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const FriendlyErrorsPlugin = require("friendly-errors-webpack-plugin")
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer")

const { HashedModuleIdsPlugin } = require("webpack")

const webpackServeWaitpage = require("webpack-serve-waitpage")

function notifierCallback(severity, errors) {
  if (severity !== "error") return

  const error = errors[0]
  const filename = error.file && error.file.split("!").pop()

  notifier.notify({
    title: require("./package.json").name,
    message: severity + ": " + error.name,
    subtitle: filename || ""
    // icon: path.join(__dirname, 'logo.png')
  })
}

module.exports = argv => {
  argv.mode = process.env.WEBPACK_SERVE
    ? "development"
    : argv.mode || "production"
  const isDev = argv.mode === "development"
  const cssLoader = isDev ? "vue-style-loader" : MiniCSSExtractPlugin.loader

  return {
    mode: argv.mode,
    context: __dirname,
    entry: {
      app: "./src/main.js"
    },
    output: {
      path: path.join(__dirname, "dist"), // $gitroot/assets
      filename: isDev ? "[name].js" : "[name].[contenthash].js",
      publicPath: "/"
    },

    serve: {
      add(app, mw, options) {
        app.use(webpackServeWaitpage(options, { theme: "material" }))
        app.use(convert(history()))
      },
      clipboard: false,
      open: true,
      host: "localhost",
      port: 8080,
      hot: true,
      dev: {
        publicPath: "/",
        writeToDisk: true
      }
    },

    resolve: {
      extensions: [".js", ".vue", ".json"],
      alias: {
        vue$: "vue/dist/vue.esm.js",
        "@": path.join(__dirname, "src")
      }
    },

    module: {
      rules: [
        {
          test: /\.vue$/,
          use: [
            {
              loader: "vue-loader",
              options: {}
            }
          ]
        },
        {
          test: /\.js$/,
          loader: "buble-loader",
          include: path.join(__dirname, "src"),
          options: {
            objectAssign: "Object.assign",
            target: {
              firefox: 52,
              chrome: 55
            }
          }
        },
        {
          test: /\.css$/,
          oneOf: [
            // this matches `<style module>`
            {
              resourceQuery: /module/,
              use: [
                cssLoader,
                {
                  loader: "css-loader",
                  options: {
                    modules: true,
                    camelCase: true,
                    localIdentName: "[local]--[hash:base64:5]"
                  }
                },
                "postcss-loader"
              ]
            },
            // this matches plain `<style>` or `<style scoped>`
            {
              use: [cssLoader, "css-loader", "postcss-loader"]
            }
          ]
        },
        {
          test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
          loader: "url-loader",
          options: {
            limit: 10000,
            name: "img/[name].[hash:7].[ext]"
          }
        },
        {
          test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
          loader: "url-loader",
          options: {
            limit: 10000,
            name: "media/[name].[hash:7].[ext]"
          }
        },
        {
          test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
          loader: "url-loader",
          options: {
            limit: 10000,
            name: "fonts/[name].[hash:7].[ext]"
          }
        }
      ]
    },
    node: {
      // prevent webpack from injecting useless setImmediate polyfill because Vue
      // source contains it (although only uses it if it's native).
      setImmediate: false,
      // prevent webpack from injecting mocks to Node native modules
      // that does not make sense for the client
      dgram: "empty",
      fs: "empty",
      net: "empty",
      tls: "empty",
      child_process: "empty"
    },
    optimization: {
      noEmitOnErrors: true,
      namedModules: isDev,
      namedChunks: isDev,
      nodeEnv: argv.mode,
      splitChunks: {
        chunks: "all",
        automaticNameDelimiter: "~",
        name: true,
        cacheGroups: {
          css: {
            test: /\.css$/,
            reuseExistingChunk: true,
            name: "style"
          },
          vendors: {
            test: /[\\/]node_modules[\\/].*\.js/s,
            priority: -10,
            enforce: true
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true
          }
        }
      }
    },
    plugins: [
      new VueLoaderPlugin(),
      new MiniCSSExtractPlugin({
        filename: "[name].[contenthash].css",
        chunkFilename: "[id].css"
      }),

      // FIXME: https://github.com/jantimon/favicons-webpack-plugin
      new HtmlWebpackPlugin({
        template: "./src/index.html",
        inject: true,
        minify: {
          removeComments: !isDev,
          collapseWhitespace: !isDev,
          removeAttributeQuotes: !isDev
          // more options:
          // https://github.com/kangax/html-minifier#options-quick-reference
        },
        chunksSortMode: isDev ? "auto" : "dependency",
        title: package.title || package.productName || package.name
      }),

      !isDev ? new HashedModuleIdsPlugin() : null,

      new FriendlyErrorsPlugin({
        onErrors: notifierCallback
      }),

      new BundleAnalyzerPlugin({
        defaultSizes: "stat",
        openAnalyzer: isDev,
        generateStatsFile: isDev,
        analyzerMode: isDev ? "server" : "static"
      })
    ].filter(Boolean)
  }
}
