import { resolve } from 'url'

// base stylesheet
// reset
import 'normalize.css/normalize.css'
// fonts
import './style/ligatures.css'
import './style/inconsolata.css'
import './style/lato.css'
import './style/varela.css'
// style
import './style/style.css'

import Vue from 'vue'

import KeenUI from 'keen-ui'
import 'keen-ui/dist/keen-ui.css'
Vue.use(KeenUI)

import App from './components/app'
import adapters from './lib/adapters'
import router from './router'

import Ligature from './components/ligature'
Vue.component('Lig', Ligature)

// eslint-disable-next-line no-unused-vars
const app = new Vue({
  el: '#app',
  data: {
    loading: true,
    loadingText: 'Initializing application, please wait...',

    load (message = '') {
      this.loading = true
      this.loadingText = message || ''
    },
    loaded () {
      this.loading = false
    }
  },

  mounted () {
    console.log('refreshing sessions...')
    this.refreshSessions()
  },

  methods: {
    refreshSessions () {
      const connections = false

      if (!connections) {
        return
      }

      const sessions = Object.assign({}, this.sessions)

      Object.values(connections).forEach(connection => {
        console.debug('refreshing session for', connection.id)
        if (sessions[connection.id]) {
          console.debug('skipping, already created')
        } else {
          console.debug('creating adapter', connection.adapter)
          const Adapter = adapters[connection.adapter]
          const adapter = new Adapter(
            Object.assign(
              { api_url: resolve(connection.params.url, 'api/v1/') },
              connection.params
            )
          )
          sessions[connection.id] = adapter
          adapter.login()
          console.debug('adapter created')
        }
      })

      this.sessions = sessions
    }
  },

  router,
  // i18n,
  render: create => create(App)
})
