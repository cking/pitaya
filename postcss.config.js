const { join } = require('path')

module.exports = ({ file, options, env }) => ({
  plugins: {
    'postcss-inject': {
      cssFilePath: join(__dirname, 'src', 'style', 'variables.css'),
      injectTo: 'fileStart'
    },
    'postcss-import': { root: file.dirname },
    'postcss-simple-extend': {}, // define placeholders that other rules can extend from

    // helper functions
    'postcss-light-text': {}, // add -webkit-font-smoothing for light text
    'postcss-momentum-scrolling': {}, // add -webkit-overflow-scrolling:touch for more natural mobile scrolling
    'postcss-ellipsis': {}, // add overflow instructions to text-overflow: ellipsis
    'postcss-input-style': {}, // add ::track and ::thumb pseudo selector

    // compatibility
    'postcss-selector-matches': {}, // :matches() => https://caniuse.com/#feat=css-matches-pseudo
    'postcss-nesting': {}, // css nesting proposal => http://tabatkins.github.io/specs/css-nesting/
    'postcss-gap': {}, // use gap, create grid-gap fallback => https://drafts.csswg.org/css-align-3/#gap-legacy
    'postcss-color-mod-function': {}, // color function => https://www.w3.org/TR/css-color-4/#funcdef-color-mod && https://github.com/jonathantneal/postcss-color-mod-function
    'postcss-css-variables': { preserve: true }, // expand css variables
    'postcss-color-hex-alpha': {}, // hex-alpha support => https://github.com/postcss/postcss-color-hex-alpha
    'postcss-color-gray': {}, // gray function => http://dev.w3.org/csswg/css-color/#grays

    // animations
    'postcss-easings': {}, // add easing functions from https://easings.net/en
    'postcss-animation': {}, // add animations from https://daneden.github.io/animate.css/
    'postcss-mq-keyframes': {}, // move keyframes to the end of the file
    'postcss-discard-overridden': {}, // remove overriden @keyframes and @counter-style [same identifier]
    'postcss-merge-idents': {}, // merge identical @keyframes (and @counter-style) [same instructions]

    // clean up & optimization
    'postcss-normalize-url': {}, // force consistent url styling
    'postcss-discard-unused': {}, // discard unsued @keyframes, @counter-style and @font
    'postcss-discard-empty': {}, // discard empty declarations and rules
    // FIXME: doeth this work with light-text and momentum-scrolling?
    'postcss-unprefix': {}, // remove vendor prefixes
    'postcss-calc': {}, // try to resolve calc functions
    'postcss-ordered-values': {}, // sort property shorthands
    'postcss-normalize-charset': {}, // single out the charset

    'postcss-emptymediaqueries': {}, // remove empty media queries
    'postcss-mq-optimize': {}, // remove invalid media queries
    'css-mqpacker': {}, // merge same media queries

    'postcss-crass': env === 'production' ? {} : false, // minify/optimize css

    // report postcss errors to the console
    'postcss-reporter': {}
  }
})
