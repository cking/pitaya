import MastodonAdapter from './mastodon'

export { default as MastodonAdapter } from './mastodon'

export default {
  mastodon: MastodonAdapter
}
